<?php

namespace App\Http\Controllers\Api;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\AutoDealer;

class ClientController extends Controller
{
    /**
     * Constructor de la clase
     */
    public function __construct()
    {

    }

    /**
     * Retorna una lista de clientes
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->input('search') != 0)
            $clients = Client::where('auto_dealer_id', $request->input('search'))->orderBy('name')->orderBy('lastname')->get();
        else
            $clients = Client::orderBy('name')->orderBy('lastname')->get();

        return $this->successResponse($clients);
    }

    /**
     * Retorna una instancia de un cliente
     *
     * @param \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return $this->successResponse($client);
    }

    /**
     * Crea una instancia de un cliente
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'nacionality' => ['required', Rule::in(['V', 'E'])],
            'document_number' => ['required', 'integer', Rule::unique('clients')],
            'address' => 'required',
            'phone' => 'required|max:50',
            'email' => 'required|email',
            'state_id' => 'required',
            'city_id' => 'required',
            'auto_dealer_id' => 'required',
        ];

        $request->validate($rules);

        $client = Client::create($request->all());

        return $this->successCreatedResponse($client);
    }

    /**
     * Actualiza una instancia de un cliente
     *
     * @param \Illuminate\Http\Request  $request
     * @param \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $rules = [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'nacionality' => [
                'required', Rule::in(['V', 'E'])
            ],
            'document_number' => [
                'required', Rule::unique('clients')->ignore($client->id)
            ],
            'address' => 'required',
            'phone' => 'required|max:50',
            'email' => 'required|email',
            'state_id' => 'required',
            'city_id' => 'required',
            'auto_dealer_id' => 'required',
        ];

        $request->validate($rules);

        $client->fill($request->all());
        $client->save();

        return $this->successResponse($client);
    }

    /**
     * Elimina una instancia de un cliente
     *
     * @param \App\Models\Client $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return $this->successResponse($client);
    }
}
