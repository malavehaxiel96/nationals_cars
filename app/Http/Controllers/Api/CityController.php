<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * Constructor de la clase
     */
    public function __construct()
    {

    }

    /**
     * Retorna una lista de ciudades
     *
     * @param \Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = City::orderBy('description')->get();

        return $this->successResponse($cities);
    }

    /**
     * Retorna una instancia de ciudad
     *
     * @param \App\Models\City $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        return $this->successResponse($city);
    }
}
