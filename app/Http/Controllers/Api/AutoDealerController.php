<?php

namespace App\Http\Controllers\Api;

use App\Models\AutoDealer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AutoDealerController extends Controller
{
     /**
     * Constructor de la clase
     */
    public function __construct()
    {

    }

    /**
     * Retorna una lista de concesionarios
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $autodealers = AutoDealer::orderBy('description')->get();

        return $this->successResponse($autodealers);
    }

    /**
     * Retorna una instancia de un concesionario
     *
     * @param \App\Models\AutoDealer $autodealer
     * @return \Illuminate\Http\Response
     */
    public function show(AutoDealer $autodealer)
    {
        return $this->successResponse($autodealer);
    }

    /**
     * Crea una instancia de un concesionario
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'description' => 'required|max:255',
            'state_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'phone' => 'required|max:50',
        ];

        $request->validate($rules);

        $autodealer = AutoDealer::create($request->all());

        return $this->successCreatedResponse($autodealer);
    }

    /**
     * Actualiza una instancia de un concesionario
     *
     * @param \Illuminate\Http\Request  $request
     * @param \App\Models\AutoDealer $autodealer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AutoDealer $autodealer)
    {
        $rules = [
            'description' => 'required|max:255',
            'state_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'phone' => 'required|max:50',
        ];

        $request->validate($rules);

        $autodealer->fill($request->all());
        $autodealer->save();

        return $this->successResponse($autodealer);
    }

    /**
     * Elimina una instancia de un concesionario
     *
     * @param \App\Models\AutoDealer $autodealer
     * @return \Illuminate\Http\Response
     */
    public function destroy(AutoDealer $autodealer)
    {
        $autodealer->delete();

        return $this->successResponse($autodealer);
    }
}
