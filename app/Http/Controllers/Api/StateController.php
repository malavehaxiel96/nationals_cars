<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\State;
use App\Http\Controllers\Controller;


class StateController extends Controller
{
    /**
     * Constructor de la clase
     */
    public function __construct()
    {

    }

    /**
     * Retorna una lista de estados
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $states = State::orderBy('description')->get();

        return $this->successResponse($states);
    }

    /**
     * Retorna una instancia de estado
     *
     * @param \App\Models\State $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        return $this->successResponse($state);
    }
}
