<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application clients.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function clients()
    {
        return view('app.clients');
    }

     /**
     * Muestra el formulario para crear un cliente
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create_client()
    {
        return view('app.clients_create');
    }

    /**
     * Muestra el formulario para editar un cliente
     *
     * @param \App\Models\Client $client
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit_client(Client $client)
    {
        return view('app.clients_edit', compact('client'));
    }

    /**
     * Muestra los concesionarios de la aplicación
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function autodealers()
    {
        return view('app.autodealers');
    }
}
