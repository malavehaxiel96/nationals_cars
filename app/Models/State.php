<?php

namespace App\Models;


class State extends Model
{
    protected $fillable = [
        'description'
    ];
}
