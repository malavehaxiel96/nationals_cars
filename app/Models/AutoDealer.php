<?php

namespace App\Models;


class AutoDealer extends Model
{
    protected $fillable = [
        'description',
        'state_id',
        'city_id',
        'address',
        'phone'
    ];
}
