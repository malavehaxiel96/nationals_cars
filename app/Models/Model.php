<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class Model extends BaseModel {

    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->created_user_id = @auth()->user()->id;
        $this->updated_user_id = @auth()->user()->id;
    }

    public function update(array $attributes = [], array $options = [])
	{
        $this->updated_user_id = @auth()->user()->id;

		return parent::update($attributes, $options);
    }

    public function delete()
    {
        $this->deleted_user_id = @auth()->user()->id;

        return parent::delete();
    }
}
