<?php

namespace App\Models;


class Client extends Model
{
    protected $fillable = [
        'name',
        'lastname',
        'nacionality',
        'document_number',
        'address',
        'phone',
        'email',
        'state_id',
        'city_id',
        'auto_dealer_id'
    ];
}
