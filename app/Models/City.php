<?php

namespace App\Models;


class City extends Model
{
    protected $fillable = [
        'description',
        'postal_code',
        'state_id'
    ];
}
