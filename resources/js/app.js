/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Push = require('push.js');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Forms


// Common

Vue.component('title-component', require('./components/common/Title.vue').default);
Vue.component('subtitle-component', require('./components/common/Subtitle.vue').default);


// Autodealers

Vue.component('autodealers-table-city-component', require('./components/pages/autodealers/AutoDealerTableCity.vue').default);
Vue.component('autodealers-table-state-component', require('./components/pages/autodealers/AutoDealerTableState.vue').default);
Vue.component('autodealers-table-component', require('./components/pages/autodealers/AutoDealerTable.vue').default);
Vue.component('autodealers-component', require('./components/pages/autodealers/Index.vue').default);


// Clients

Vue.component('client-search-by-autodealer-component', require('./components/pages/clients/ClientSearchByAutodealer.vue').default);
Vue.component('select-city-component', require('./components/pages/clients/SelectCity.vue').default);
Vue.component('select-state-component', require('./components/pages/clients/SelectState.vue').default);
Vue.component('select-autodealer-component', require('./components/pages/clients/SelectAutoDealer.vue').default);
Vue.component('client-form-edit-component', require('./components/pages/clients/ClientFormEdit.vue').default);
Vue.component('client-edit-component', require('./components/pages/clients/Edit.vue').default);
Vue.component('client-form-component', require('./components/pages/clients/ClientForm.vue').default);
Vue.component('client-create-component', require('./components/pages/clients/Create.vue').default);
Vue.component('client-table-autodealer-component', require('./components/pages/clients/ClientTableAutoDealer.vue').default);
Vue.component('client-table-component', require('./components/pages/clients/ClientTable.vue').default);
Vue.component('client-component', require('./components/pages/clients/Index.vue').default);


// Layouts

Vue.component('footer-component', require('./components/layouts/Footer.vue').default);
Vue.component('sidebar-footer-component', require('./components/layouts/menu/SidebarFooter.vue').default);
Vue.component('sidebar-menu-component', require('./components/layouts/menu/SidebarMenu.vue').default);
Vue.component('sidebar-search-component', require('./components/layouts/menu/SidebarSearch.vue').default);
Vue.component('sidebar-user-component', require('./components/layouts/menu/SidebarUser.vue').default);
Vue.component('sidebar-brand-component', require('./components/layouts/menu/SidebarBrand.vue').default);
Vue.component('buttonexpand-component', require('./components/layouts/menu/ButtonExpand.vue').default);
Vue.component('sidebar-component', require('./components/layouts/menu/Sidebar.vue').default);
Vue.component('menu-component', require('./components/layouts/menu/Index.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
});


require('./scripts');
