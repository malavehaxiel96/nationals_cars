@extends('layouts.auth')

@section('content')
    <h3>Iniciar sessión</h3>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong style="color:red">{{ $message }}</strong>
            </span>
        @enderror

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong style="color:red">{{ $message }}</strong>
            </span>
        @enderror

        <div class="form-group" style="margin-top:20px">
            <input id="email" type="email" class="@error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email">
        </div>

        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">
        </div>

        <div class="form-group row mb-0">
            <button id="login-button" type="submit" class="btn btn-primary">
                {{ __('Login') }}
            </button>
        </div>

        <div class="form-group" style="margin-top:10px">
            <a class="btn" href="{{ route('register') }}" style="text-decoration: none">{{ __('Register') }}</a>
        </div>
    </form>
@endsection
