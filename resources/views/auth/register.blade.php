@extends('layouts.auth')

@section('content')
    <h3>Registro</h3>

    <form method="POST" action="{{ route('register') }}">
        @csrf

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong style="color:red">{{ $message }}</strong>
            </span>
        @enderror

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong style="color:red">{{ $message }}</strong>
            </span>
        @enderror

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong style="color:red">{{ $message }}</strong>
            </span>
        @enderror

        <div class="form-group" style="margin-top:20px">
            <input id="name" type="name" class="@error('name') is-invalid @enderror" placeholder="Name" name="name" value="{{ old('name') }}" required autocomplete="name">
        </div>

        <div class="form-group">
            <input id="email" type="email" class="@error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email">
        </div>

        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">
        </div>

        <div class="col-md-6">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar password" required autocomplete="new-password">
        </div>

        <div class="form-group row mb-0">
            <button id="login-button" type="submit" class="btn btn-primary">
                {{ __('Register') }}
            </button>
        </div>

        <div class="form-group" style="margin-top:10px">
            <a class="btn" href="{{ route('login') }}" style="text-decoration: none">{{ __('Login') }}</a>
        </div>
    </form>
@endsection
