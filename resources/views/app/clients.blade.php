@extends('layouts.app')

@section('content')
    @php
        $url = json_encode(url("/"));
    @endphp

    <client-component :url="{{ $url }}">></client-component>
@endsection
