@extends('layouts.app')

@section('content')
    @php
        $url = json_encode(url("/"));
    @endphp

    <autodealers-component :url="{{ $url }}"></autodealers-component>
@endsection
