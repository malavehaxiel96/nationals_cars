@extends('layouts.app')

@section('content')
    @php
        $url = json_encode(url("/"));
    @endphp

    <client-create-component :url="{{ $url }}"></client-create-component>
@endsection
