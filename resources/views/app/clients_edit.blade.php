@extends('layouts.app')

@section('content')
    @php
        $url = json_encode(url("/"));
    @endphp

    <client-edit-component
        :client_id="{{ $client->id }}"
        :url="{{ $url }}">
    </client-edit-component>
@endsection
