<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.13/css/all.css'>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="page-wrapper chiller-theme toggled">
            @if (Auth::check())
                @php
                    $url = json_encode(url("/"));
                    $auth_name = json_encode(auth()->user()->name);
                    $auth_email = json_encode(auth()->user()->email);
                    $auth_token = json_encode(auth()->getSession()->token());
                @endphp

                <menu-component
                    :url="{{ $url }}"
                    :auth_name="{{ $auth_name }}"
                    :auth_email="{{ $auth_email }}"
                    :auth_token="{{ $auth_token }}">
                </menu-component>
            @endif

            <!-- sidebar-wrapper  -->
            <main class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            @yield('content')
                        </div>

                        <footer-component></footer-component>
                    </div>
                </div>
            </main>
        </div>
    </div>
</body>
</html>
