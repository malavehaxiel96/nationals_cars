<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


# RUTAS PARA LA ADMINISTRACIÓN DE RECURSOS DE LA APLICACION

Route::middleware(['auth'])->prefix('api')->group(function () {

    Route::apiResource('states', 'Api\StateController', ['only' => ['index', 'show']]);
    Route::apiResource('cities', 'Api\CityController', ['only' => ['index', 'show']]);
    Route::apiResource('autodealers', 'Api\AutoDealerController');
    Route::apiResource('clients', 'Api\ClientController');

});

# RUTAS PARA LOS MUDULOS DE LA APP

Route::get('/clients', 'HomeController@clients');
Route::get('/clients/create', 'HomeController@create_client');
Route::get('/clients/edit/{client}', 'HomeController@edit_client');

Route::get('/autodealers', 'HomeController@autodealers');
