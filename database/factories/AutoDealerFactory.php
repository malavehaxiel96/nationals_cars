<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AutoDealer;
use App\Models\City;
use Faker\Generator as Faker;

$factory->define(AutoDealer::class, function (Faker $faker) {

    $city = City::findOrfail($faker->numberBetween(1, 498));

    return [
        'description' => $faker->company,
        'city_id' => $city->id,
        'state_id' => $city->state_id,
        'address' => $faker->address,
        'phone' => $faker->tollFreePhoneNumber,
    ];
});
