<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\City;
use App\Models\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {

    $city = City::findOrfail($faker->numberBetween(1, 498));

    return [
        'name' => $faker->firstName,
        'lastname' => $faker->lastName,
        'nacionality' => $faker->randomElement(['V', 'E']),
        'document_number' => $faker->numberBetween(5000000, 20000000),
        'address' => $faker->address,
        'phone' => $faker->tollFreePhoneNumber,
        'email' => $faker->email,
        'city_id' => $city->id,
        'state_id' => $city->state_id,
        'auto_dealer_id' => $faker->numberBetween(1, 15),
    ];
});
