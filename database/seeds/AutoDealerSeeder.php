<?php

use App\Models\AutoDealer;
use Illuminate\Database\Seeder;

class AutoDealerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crea 15 registros ficticios de concesionarios
        factory(AutoDealer::class, 15)->create();
    }
}
