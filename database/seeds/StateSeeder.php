<?php

use App\Models\State;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Lista de los Estados de Venezuela

        $states = collect([
            'Amazonas',
            'Anzoátegui',
            'Apure',
            'Aragua',
            'Barinas',
            'Bolívar',
            'Carabobo',
            'Cojedes',
            'Delta Amacuro',
            'Falcón',
            'Guárico',
            'Lara',
            'Mérida',
            'Miranda',
            'Monagas',
            'Nueva Esparta',
            'Portuguesa',
            'Sucre',
            'Táchira',
            'Trujillo',
            'Vargas',
            'Yaracuy',
            'Zulia',
            'Distrito Capital',
            'Dependencias Federales',
        ]);

        // Llenar tabla de states
        foreach ($states as $value)
        {
            State::create([
                'description' => $value,
            ]);
        }
    }
}
